#!/bin/bash

USER="USER"
PASSWORD="PASS"
OUTPUT="DIR"

rm -f $OUTPUT/*.sql

databases=`mysql -u $USER -p$PASSWORD -e "SHOW DATABASES;" | tr -d "| " | grep -v Database`

for db in $databases; do
    if [[ "$db" != "information_schema" ]] && [[ "$db" != _* ]] ; then
        echo "Dumping database: $db"
        mysqldump --force --opt -u $USER -p$PASSWORD --databases $db > $OUTPUT/$db.sql
    fi
done
cd $OUTPUT
git add -A && git commit -m "Backup db: $(date)" && git push
